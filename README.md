[ ![Codeship Status for repworth/sdk-for-java](https://codeship.com/projects/06254cd0-92e1-0133-6068-5afc0a2ac2da/status?branch=master)](https://codeship.com/projects/124773)

## Description
The Repworth SDK for Java.

## Install

#### Maven
```xml
<dependencies>
    <dependency>
        <groupId>com.repworth</groupId>
        <artifactId>sdk-for-java</artifactId>
        <version>{version}</version>
    </dependency>
    ...
</dependencies>
```

#### SBT
```scala
libraryDependencies += "com.repworth" % "sdk-for-java" % {version}
```

## Develop

#### Software
|name|
|---|
|[docker toolbox](https://www.docker.com/docker-toolbox)|
|[maven](https://maven.apache.org/install.html)|
|[codeship jet](https://codeship.com/documentation/docker/installation/)|

#### Scripts

##### compile & test
```shell
mvn verify
```

##### publish
```shell
jet steps
```
